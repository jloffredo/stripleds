/*
*  stripLEDs.ino
*
*  Arduino sketch to control a strip light with various serial commands.
*  Uses Adafuit Neopixel library
*  https://github.com/adafruit/Adafruit_NeoPixel
*
*  Serial commands:
*     * All commands must end with a '>'
*     * Commands are of the format of a single letter followed by
*       numeric values
*  n              Get the number of pixels
*  a:r:g:b        A color to add to the strip with the decimal RGB values
*  d:p            Adjust the brightness of the lights to a percent value.
*                 Returns GO[d]:p
*  g:n            Change the light groupings, the number of lights of the
*                 color to display in a row.
*  r:n            Remove the color in the index represented by n.
*  t:n            Change time delay
*/

#include <Adafruit_NeoPixel.h>

// Set the pin number on arduino
#define PIN 2
// Set the number of pixels
#define LEN 300

// Initialize strip
Adafruit_NeoPixel strip = Adafruit_NeoPixel(300, PIN, NEO_GRB + NEO_KHZ800);

/*
*  Globals
*/
uint16_t groupings = 3;    // number of groupings to display
uint16_t colorCnt = 2;     // number of colors
uint32_t colors[21];       // color array
uint16_t bright = 60;      // brightness

char inData[20];           // command data
byte index = 0;            // index
uint8_t pcount = 0;        // number of paremeters
uint8_t ppos[10];          // position of paremeters

uint8_t effects = 0;       // number representing effect

uint16_t delayTime = 50;    // amount of time between delay
uint16_t color = 0;        // track color for rainbow effects

/*
*  Setup
*/
void setup() 
{
   strip.begin();
   strip.show();

   Serial.begin(9600);

   // add initial colors
   colors[0] = strip.Color(255,74,0);
   colors[1] = strip.Color(0,33,165);
   colors[2] = '\0';

   strip.setBrightness(bright * 255 / 100);
   lightIt();
}

void loop() 
{
   /*
   * Listens for serial data.
   * Calls the appropriate command when a > character
   * is recieved signifying end of command
   */
   char inChar = -1;          
   while(Serial.available() > 0) 
   {
      if ( index < sizeof(inData) )
      {
         inChar = Serial.read();
         if ( inChar != '>' ) // Reached end of command data
         {
            inData[index] = inChar;
            index++;
            if ( inChar == ':' ) // Add value count
            {
               ppos[pcount] = index;
               pcount++;
            }
         }
         else
         {
            ppos[pcount] = index; 
         }
      }
   }

   // Check and run the command
   if ( index > 0 && inChar == '>' )
   {
      inData[index] = '\0';
      //Serial.print("sent: ");
      //Serial.println(inData );
      switch (inData[0]) 
      {
         case 'n':   // get number of pixels
            Serial.print("OK[n]:");
            Serial.println(strip.numPixels());
            break;
         case 'd':   // set the brightness
            dimStrip();
            break;
         case 'a':   // add a color
            addColor();
            break;
         case 'r':   // remove a color
            removeColor();
            break;
         case 'g':   // change grouping
            changeGroupings();
            break;
         case 'e': // set effect
            setEffect();
            break;
         case 't': // set delay
            setDelay();
            break;
         case 's': // return status
            echoStatus();
            break;
      }
      index = 0;
      pcount = 0;
   }
   effect(); // execute effect - not implemented yet
}

/*
*  Functions
*/

/*
*  echoStatus
*
*  print out the status of the light strip
*/

void echoStatus()
{
   char out[20];
   //sprintf(out,"%i,%i,%i",r,g,b);
   //Serial.println(out);

   // Brightness
   sprintf(out,"d:%i;",bright);
   Serial.print(out);

   sprintf(out,"g:%i;",groupings);
   Serial.print(out);

   sprintf(out,"e:%i;",effects);
   Serial.print(out);

   sprintf(out,"t:%i;",delayTime);
   Serial.print(out);

   for( int i = 0; i < colorCnt; i++ )
   {
      uint32_t color = colors[i];
      uint8_t r = (color >> 16);
      uint8_t g = (color >> 8);
      uint8_t b = color;
      sprintf(out,"c[%i]:%i:%i:%i;",i,r,g,b);
      Serial.print(out);
   }

   Serial.println();
}

/*
*  setDelay
*
*  sets the timing of transitions
*/
void setDelay()
{
   if ( pcount == 1 )
   {
      delayTime = getNumber(0);
   }
   Serial.print("OK[t]:");
   Serial.println(delayTime);
}
/*
*  setEffect
*  
*  sets the effects
*  1 chaser
*/
void setEffect()
{
   if ( pcount == 1 )
   {
      effects = getNumber(0);
      Serial.print("OK[e]:");
      Serial.println(effects);
   }
}


/*
*  changeGroupings
*
*  set the number in groupings and reset the lights
*  prints the current number of groupings if no parameters
*/
void changeGroupings() 
{
   if ( pcount == 1 )
   {
      groupings = getNumber(0);
      resetLights();
      lightIt();
   }
   Serial.print("OK[g]:");
   Serial.println(groupings);
}

/*
*  dimStrip
*
*  set the brightness level
*  prints the current brightness if no parameters
*/
void dimStrip() 
{
   if ( pcount == 1 )
   {
      bright = getNumber(0);
      strip.setBrightness(bright * 255 / 100);
      strip.show();
   }

   Serial.print("OK[d]:");
   Serial.println( bright );
}


/*
*  addColor
*
*  add a color and reset the strip
*/
void addColor() 
{
   if ( pcount == 3 && colorCnt < 20 )
   {
      uint8_t r = getNumber(0);
      uint8_t g = getNumber(1);
      uint8_t b = getNumber(2);
  
      colors[colorCnt] = strip.Color(r,g,b);
      colorCnt++;
      resetLights();
  
      lightIt();
      Serial.print("OK[a]:");
      char out[12];
      sprintf(out,"%i,%i,%i",r,g,b);
      Serial.println(out);
   }
}

/*
*  resetLights
*  
*  just resets the brightness - not necessary but it makes for a fun effect
*/
void resetLights() 
{
   strip.setBrightness(0);
   strip.setBrightness(bright * 255 / 100);
}

/*
*  removeColor
*
*  remove a color at a given position
*/
void removeColor() {
   if ( pcount == 1 )
   {
      uint8_t pos = getNumber(0);
      for( int i = pos; i < colorCnt; i++ )
      {
         colors[i] = colors[i+1];
      }
      colorCnt--;
      Serial.print("OK[r]:");
      Serial.println(pos);
  
      resetLights();
  
      lightIt();
   }
}

/*
*  lightIt
*
*  lights up the strip
*/
void lightIt() 
{
   uint16_t pi = 0;
   uint16_t limit = round(strip.numPixels()/groupings)+1;
   for ( int i = 0; i < limit && pi < strip.numPixels(); i++ )
   {
      uint16_t cc = i%colorCnt;
    
      for ( int j = 0; j < groupings; j++ )
      {
         strip.setPixelColor(pi, colors[cc]);
         pi++;
         strip.show();
         delay(10);
      }
   } 
}

/*
*  effect
*
*  adds an animated effect to the lights
*/
void effect() 
{ 
   // shift them
   switch ( effects ) 
   {
      case 1: // reset 
         resetLights();
         lightIt();
         effects=0;
         break;
      case 2:
         chaser();
         break;
      case 3:
         rainbow();
         break;
      case 4:
         rainbowCycle();
         break;
      case 5:
         theaterChaseRainbow();
         break;
      case 6:
         theaterChase();
         break;
   }
}

void chaser()
{
   strip.setBrightness(255); // have to set brightness to 100% to get accurate color
   uint32_t zero = strip.getPixelColor(0);
   for ( int i = 0; i < strip.numPixels(); i++ )
   {
      if ( i == strip.numPixels() -1 )
      {
         strip.setPixelColor(i,zero);
      }
      else 
      {
         strip.setBrightness(255); // have to set brightness to 100% to get accurate color
         uint32_t next = strip.getPixelColor(i+1);
         strip.setPixelColor(i,next);
      }
      strip.setBrightness(bright * 255 /100);
      strip.show();
      delay(delayTime);
   }
}

/*
* Utility functions
*/

/*
*  getNumber
*
*  get the specified paremeter from the input data
*/
uint16_t getNumber(uint8_t pos) 
{
   uint16_t num = 0;
   uint8_t indx = 0;
   uint8_t ints[5];
   uint8_t fu = 1;
  
   if ( pos+1 == pcount ) 
   {
      fu = 0;
   }
  
   // get the values of the requested paremter and store them as ints
   for ( int i = ppos[pos]; i < (ppos[pos+1]-fu); i++ ) 
   {
      ints[indx] = inData[i]-'0';
      indx++;
   }
  
   // calculate the number
   for ( int i = 0; i < indx; i++ ) 
   {
      num += ints[indx-1-i] * pow(10,i);
   }
  
  return num;
}

/*
* From AdaFruit NeoPixel StrandTest
*/
void rainbow()
{
   uint16_t i;

   for(i=0; i<strip.numPixels(); i++) 
   {
      strip.setPixelColor(i, Wheel((i+color) & 255));
   }
   strip.show();
   delay(delayTime);

   color++;
   if( color == 255 )
   {
      color = 0;
   }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle() 
{
   uint16_t i;

   for(i=0; i< strip.numPixels(); i++) 
   {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + color) & 255));
   }

   strip.show();
   delay(delayTime);

   color++;
   if ( color == 255 )
   {
      color = 0;
   }
}

//Theatre-style crawling lights.
void theaterChase()
{
   uint16_t idx = 0;
   for (int q=0; q < 3; q++) 
   {
      for (int i=0; i < strip.numPixels(); i=i+3) 
      {
         strip.setPixelColor(i+q, colors[idx]);    //turn every third pixel on
      }
      strip.show();
     
      delay(delayTime);
     
      for (int i=0; i < strip.numPixels(); i=i+3) 
      {
         strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
         idx++;
         if ( idx == colorCnt )
         {
            idx = 0;
         }
   }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow() 
{
   for (int q=0; q < 3; q++) 
   {
      for (int i=0; i < strip.numPixels(); i=i+3) 
      {
         strip.setPixelColor(i+q, Wheel( (i+color) % 255));    //turn every third pixel on
      }
      strip.show();
       
      delay(delayTime);
       
      for (int i=0; i < strip.numPixels(); i=i+3) 
      {
         strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
   }

   color++;
   if ( color == 255 )
   {
      color = 0;
   }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) 
  {
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } 
  else if(WheelPos < 170) 
  {
   WheelPos -= 85;
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } 
  else 
  {
   WheelPos -= 170;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
